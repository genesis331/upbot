var admin = require('firebase-admin');
var { deleteFile } = require('../functions/storage');

var testSecrets;
try {
	testSecrets = require(`../secrets.json`);
} catch(error) {}

var privatekey1;
if (process.env.private_key) {
	privatekey1 = process.env.private_key.replace(/\\n/g, '\n');
}

var databaseProp = {
	"type": "service_account",
	"project_id": process.env.project_id || testSecrets.project_id,
	"private_key_id": process.env.private_key_id || testSecrets.private_key_id,
	"private_key": privatekey1 || testSecrets.private_key,
	"client_email": process.env.client_email || testSecrets.client_email,
	"client_id": process.env.client_id || testSecrets.client_id,
	"auth_uri": "https://accounts.google.com/o/oauth2/auth",
	"token_uri": "https://oauth2.googleapis.com/token",
	"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
	"client_x509_cert_url": process.env.client_x509_cert_url || testSecrets.client_x509_cert_url
};

admin.initializeApp({
	credential: admin.credential.cert(databaseProp),
	databaseURL: process.env.databaseURL || testSecrets.databaseURL
});

var db = admin.database();

module.exports = {
    pingDatabase: () => {
        db.ref("/test").once("value", function(snapshot) {
            console.log("From database: " + snapshot.val());
        });
    },
    generateUploadKey: (amount, ts, generatedLink, authorId) => {
		var refKey = db.ref("/upKey");
		refKey.once("value", function(snapshot) {
			let writeKey = {
				"hrsAvailable": amount,
		    	"timeStamp": ts,
		    	"linkKey": generatedLink,
		    	"fileKey": "",
		    	"authorId": authorId
			}
			if (snapshot.val() == undefined || snapshot.val().length == 0) {
				let output = [];
				output.push(writeKey);
				refKey.set(output);
			} else {
				let output = snapshot.val();
				output.push(writeKey);
				refKey.set(output);
			}
        });
	},
	validateKey,
	checkRecord,
	recordFileKey,
	recordFileId,
	getFileKey,
	getFileId,
	checkExpiry
}

async function validateKey(key) {
	return new Promise((resolve) => {
		db.ref("/upKey").once("value", function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['linkKey'] == key) {
					resolve(true);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['linkKey'] != key) {
						resolve(false);
					}
				}
			}
		});
	});
}

async function checkRecord(key) {
	return new Promise((resolve) => {
		db.ref("/upKey").once("value", function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['linkKey'] == key) {
					resolve(snapshot.val()[i]);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['linkKey'] != key) {
						resolve(false);
					}
				}
			}
		});
	});
}

async function recordFileKey(checkKey, fileKey, file) {
	const promises = [];
	promises.push(new Promise((resolve) => {
		var refKey1 = db.ref("/fileKey");
		refKey1.once("value", function(snapshot) {
			let writeKey = {
				"fileKey": fileKey,
				"fileName": file.filename,
				"fileId": ""
			}
			if (snapshot.val() == undefined || snapshot.val().length == 0) {
				let output = [];
				output.push(writeKey);
				refKey1.set(output);
				resolve(true);
			} else {
				let output = snapshot.val();
				output.push(writeKey);
				refKey1.set(output);
				resolve(true);
			}
		});
	}));
	promises.push(new Promise((resolve) => {
		var refKey2 = db.ref("/upKey");
		refKey2.once("value", function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['linkKey'] == checkKey.linkKey) {
					let output = snapshot.val();
					let updateData = {
						"authorId": checkKey.authorId,
						"fileKey": fileKey,
						"hrsAvailable": checkKey.hrsAvailable,
						"linkKey": checkKey.linkKey,
						"timeStamp": checkKey.timeStamp,
					}
					output[i] = updateData;
					refKey2.set(output);
					resolve(true);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['linkKey'] != checkKey.linkKey) {
						resolve(false);
					}
				}
			}
		});
	}));
	return Promise.all(promises);
}

async function recordFileId(fileKey, downloadURL) {
	return new Promise((resolve) => {
		var refKey = db.ref("/fileKey");
		refKey.once("value", function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['fileKey'] == fileKey) {
					let output = snapshot.val();
					let updateData = {
						"fileId": downloadURL,
						"fileKey": snapshot.val()[i].fileKey,
						"fileName": snapshot.val()[i].fileName,
					}
					output[i] = updateData;
					refKey.set(output);
					resolve(true);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['fileKey'] != fileKey) {
						resolve(false);
					}
				}
			}
		});
		resolve(true);
	});
}

async function getFileKey(key) {
	return new Promise((resolve) => {
		var refKey = db.ref("/upKey");
		refKey.once("value", function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['linkKey'] == key) {
					resolve(snapshot.val()[i]['fileKey']);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['linkKey'] != key) {
						resolve(false);
					}
				}
			}
		});
	});
}

async function getFileId(fileKey) {
	return new Promise((resolve) => {
		var refKey = db.ref("/fileKey");
		refKey.once("value", async function(snapshot) {
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['fileKey'] == fileKey) {
					resolve(snapshot.val()[i]['fileId']);
				}
				if (i == snapshot.val().length - 1) {
					if (snapshot.val()[i]['fileKey'] != fileKey) {
						resolve(false);
					}
				}
			}
		});
	});
}

function deleteFileKey(key) {
	var refKey = db.ref("/fileKey");
	refKey.once("value", function(snapshot) {
		if (snapshot.val() !== null) {
			let temp = [];
			temp.push(snapshot.val());
			for (let i = 0; i <= snapshot.val().length - 1; i++) {
				if (snapshot.val()[i]['fileKey'] == key) {
					temp.splice(i, 1);
					refKey.set(temp);
				}
			}
		}
	});
}

async function checkExpiry() {
	const now = Date.now();
	var refKey = db.ref("/upKey");
	return new Promise(async (resolve) => {
		refKey.once("value", async function(snapshot) {
			if (snapshot.val() !== null) {
				let temp = [];
				temp.push(snapshot.val());
				for (let i = 0; i <= snapshot.val().length - 1; i++) {
					var time = snapshot.val()[i]['timeStamp'];
					var expirationTime = time + (snapshot.val()[i]['hrsAvailable'] * 3600000);
					if (now > expirationTime) {
						deleteFileKey(snapshot.val()[i]['fileKey']);
						await deleteFile(snapshot.val()[i]['fileKey']);
						temp.splice(i, 1);
						refKey.set(temp);
					}
				}
			}
			resolve(true);
		});
	});
}
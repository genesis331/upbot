var { generateUploadKey } = require('../functions/database');

module.exports = {
	name: 'upload',
    description: 'Opens a browser for the user to browse the file to be uploaded.',
    usage:'<num of hrs the file is available (1-4)>',
	guildOnly: false,
	cooldown : 300,
	minHrA: 1,
	maxHrA: 4,
	args:true,
	execute(message, args) {
		function getRandomInt( min, max ) {
			return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
		}
		
		function generateKey() {
		var tokens = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
		chars = 5,
		segments = 3,
		keyString = "";
			
		for( var i = 0; i < segments; i++ ) {
			var segment = "";
			
			for( var j = 0; j < chars; j++ ) {
				var k = getRandomInt( 0, 35 );
				segment += tokens[ k ];
			}
			
			keyString += segment;
			
			if( i < ( segments - 1 ) ) {
				keyString += "-";
			}
		}
		
		return keyString;
		}

		function generateFileKey() {
			var length = 30;
			var result = '';
			var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
			var charactersLength = characters.length;
			for ( var i = 0; i < length; i++ ) {
				result += characters.charAt(Math.floor(Math.random() * charactersLength));
			}
		return result;
		}

		var amount= parseInt(args[0]);

		if (isNaN(amount)) {
			return message.reply('that doesn\'t seem to be a valid number.');
		} else if (amount < this.minHrA || amount > this.maxHrA) {
			return message.reply(`you can only upload a file that lasts between ${this.minHrA} and ${this.maxHrA} hours long.`);
		}

	    let ts = Date.now();

	   	let date_ob = new Date(ts);
		let date = date_ob.getDate();
		let month = date_ob.getMonth() + 1;
		let year = date_ob.getFullYear();
		let hours = date_ob.getHours();
		let minutes = date_ob.getMinutes();


	   var generatedTime = `${date}/${month}/${year} ${hours}:${minutes}`;
	   
	   var generatedLink = generateKey();
	   var generatedFile = generateFileKey();

	   generateUploadKey(amount, ts, generatedLink, message.author.id);
	   
	   var redirect = `http://upbot.ga/#/upload?key=${generatedLink}`;
	   var retrieve = `http://upbot.ga/#/retrieve?key=${generatedLink}`;
	   const linkEmbed = {
			color: 0xff5315,
			title: 'upBot Upload Link',
			fields: [
				{
					name: `Generated at ${generatedTime}`,
					value: `[Click me to go to link](${redirect})`,
				},
				{
					name: `Number of hours available : `,
					value: `${amount}`,
				}
			],
	   };
	   const uploadEmbed = {
		   color: 0xff5315,
		   fields: [
			   {
					name :`\u200b`,  
					value: `${message.author}, upload link has been sent to your DM. The file will last for ${amount} hr(s) long. The download link will only work after the upload completes.`,
			   },
			   {
			   		name:`\u200b`,
					value: `[Click here for the download file link](${retrieve})`,
			   },
		   ]
	   }

		message.channel.send({embed:uploadEmbed});
		message.author.send({embed: linkEmbed});
	},
};

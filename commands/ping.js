module.exports = {
	name: 'ping',
    description: 'Ping!',
    usage:'<command>',
	cooldown: 3,
    guildOnly: false,
	execute(message, args) {
		message.channel.send('Pong.');
	},
};

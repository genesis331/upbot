const fs = require('fs');
var { validateKey } = require('./functions/database');
var { uploadToStorage, generateFileKey, generateDownloadLink } = require('./functions/storage');
const express = require('express');
const multer = require('multer');
const app = express();

app.get('/', (req, res) => {
	fs.readFile('./index.html', null, function (error, data) {
		res.writeHead(200);
		res.write(data);
		res.end();
	});
});

app.get('/verify', async (req, res) => {
	if (await validateKey(req.query.key)) {
		console.log("------------------------------");
		console.log("Verify API called.");
		console.log("Key:", req.query.key);
		console.log("------------------------------","\n");
		res.sendStatus(200);
	} else {
		res.sendStatus(400);
	}
});

let currentFileKey = generateFileKey();

var storage = multer.diskStorage({
	filename: function (req, file, cb) {
		cb(null, currentFileKey + '-' + file.originalname);
	}
  });
var upload = multer({ storage: storage });

app.post('/upload', upload.single('file'), async (req, res) => {
	const file = req.file;
	if (await validateKey(req.query.key)) {
		console.log("------------------------------");
		console.log("Upload API called.");
		console.log("Key:", req.query.key);
		console.log("------------------------------","\n");
		if (await uploadToStorage(req.query.key, file, currentFileKey)) {
			res.sendStatus(200);
			currentFileKey = generateFileKey();
		} else {
			res.sendStatus(500);
		}
	} else {
		res.sendStatus(400);
	}
});

app.get('/retrieve', async (req, res) => {
	if (await validateKey(req.query.key)) {
		console.log("------------------------------");
		console.log("Retrieve API called.");
		console.log("Key:", req.query.key);
		console.log("------------------------------","\n");
		let downloadLink = await generateDownloadLink(req.query.key);
		if (downloadLink !== "") {
			if (downloadLink) {
				res.status(200).json({ downloadLink: downloadLink });
			}
		} else {
			res.sendStatus(204);
		}
	} else {
		res.sendStatus(400);
	}
});

app.listen(process.env.PORT || 3000);